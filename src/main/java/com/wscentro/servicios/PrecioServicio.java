package com.wscentro.servicios;

import java.util.List;

import com.wscentro.modelos.Precio;
import com.wscentro.modelos.RespuestaProcedimiento;

public interface PrecioServicio {
	
	public List<Precio> findAll();
	
	public List<Precio>  buscarPorFecha(String fecha);
	
	public RespuestaProcedimiento registrarPrecioEstacion(Precio precio);
	
	public RespuestaProcedimiento eliminarPrecioEstacion(Precio precio);

}
