package com.wscentro.servicios;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
 
import com.wscentro.modelos.BodyActMov;
import com.wscentro.modelos.RespuestaEmpresa;
import com.wscentro.modelos.RespuestaResult;
import com.wscentro.modelos.objectoCR;
import com.wscentro.repositorios.IMovimientoRepositorios;


@Service
public class MovimientoServicioImp implements MovimientoServicio{
	
	@Autowired
	private IMovimientoRepositorios movimientoRepo;
	
	@Override
	public RespuestaResult registrarPrecioEstacion(objectoCR movimiento) {
		// TODO Auto-generated method stub
		return movimientoRepo.registrarPrecioEstacion(movimiento.getNummov(), movimiento.getProducto(), movimiento.getIeps(), movimiento.getPrecio());
	}

	@Override
	public RespuestaResult actualizarMovimientosFactura(BodyActMov movimiento) {
		// TODO Auto-generated method stub
		return movimientoRepo.actualizarMovimientosFactura( movimiento.getFactura(), movimiento.getMovs() );
	}

	@Override
	public RespuestaEmpresa Config_DuplicarEmpresa(String empresa, String cuenta) {
		// TODO Auto-generated method stub
		RespuestaEmpresa respuesta = new RespuestaEmpresa();
		try {
			respuesta.setError(false);
			respuesta.setMsg(	movimientoRepo.Config_DuplicarEmpresa(empresa, cuenta).trim() );	
 		} catch (Exception e) {
    		 respuesta.setError(true);
			 respuesta.setMsg( e.getCause().getCause().getMessage().replace("\n", "").replace("  ", "") );
		}

		return respuesta;
		
	}

}
