package com.wscentro.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wscentro.modelos.Precio;
import com.wscentro.modelos.RespuestaProcedimiento;
import com.wscentro.repositorios.IPrecioRepositorio;

@Service
public class PrecioServicioImp implements PrecioServicio{
	@Autowired
	private IPrecioRepositorio precioRepo;
	
	@Override
	public List<Precio> findAll() {
		return precioRepo.findAll();
	}

	@Override
	public List<Precio> buscarPorFecha(String fecha) {
		return precioRepo.buscarPorFecha(fecha);
	}

	@Override
	public RespuestaProcedimiento registrarPrecioEstacion(Precio precio) {
		return precioRepo.registrarPrecioEstacion(precio.getFecha(),
				  precio.getEstacion(),
				  precio.getMagna(),
				  precio.getPremium(),
				  precio.getDiesel());
	
	}

	@Override
	public RespuestaProcedimiento eliminarPrecioEstacion(Precio precio) {
		return precioRepo.eliminarPrecioEstacion(precio.getFecha(), precio.getEstacion());
	}



	 

}
