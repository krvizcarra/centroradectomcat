package com.wscentro.servicios;


import com.wscentro.modelos.BodyActMov;
import com.wscentro.modelos.RespuestaEmpresa;
import com.wscentro.modelos.RespuestaResult;
import com.wscentro.modelos.objectoCR;

public interface MovimientoServicio {
	
	public RespuestaResult registrarPrecioEstacion(objectoCR movimiento);
	public RespuestaResult actualizarMovimientosFactura(BodyActMov movimiento);
	public RespuestaEmpresa Config_DuplicarEmpresa(String empresa, String cuenta);

}
