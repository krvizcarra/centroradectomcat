package com.wscentro.modelos;

public interface RespuestaResult {
	public String getMsg();
	public int getError();	
}
