package com.wscentro.modelos;

public class empresa {
    protected String empresa;
    protected String cuenta;

    public empresa(){
        this.empresa = "";
        this.cuenta = "";
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    
}
