package com.wscentro.modelos;

public class RespuestaEmpresa {
    
    protected boolean error;
	protected String msg;
	
	public RespuestaEmpresa() {
		this.error = true;
		this.msg = "";
	}

	public boolean getError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
    
}
