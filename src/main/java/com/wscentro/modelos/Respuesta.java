package com.wscentro.modelos;

import java.util.List;

public class Respuesta {
	protected int error;
	protected String msg;
	protected List<Precio> precios;
	
	public Respuesta() {
		this.error = 1;
		this.msg = "";
		this.precios = null;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public List<Precio> getPrecios() {
		return precios;
	}

	public void setPrecios(List<Precio> precios) {
		this.precios = precios;
	}
	
	
}
