package com.wscentro.modelos;

public class objectoCR {
	protected int nummov;
	protected int producto;
	protected double ieps;
	protected double precio;

	public objectoCR() {
		this.nummov = 0;
		this.producto = 0;
		this.ieps = 0;
		this.precio = 0;
	}

	public int getNummov() {
		return nummov;
	}

	public void setNummov(int nummov) {
		this.nummov = nummov;
	}

	public int getProducto() {
		return producto;
	}

	public void setProducto(int producto) {
		this.producto = producto;
	}

	public double getIeps() {
		return ieps;
	}

	public void setIeps(double ieps) {
		this.ieps = ieps;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	
}
