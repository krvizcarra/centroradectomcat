package com.wscentro.modelos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name= "TPreciosRegiones_v2")
public class Precio extends Identificador{

	private static final long serialVersionUID = 1L;



	public Precio() {
		this.fecha = "";
		this.estacion = 0;
		this.magna = 0;
		this.premium = 0;
		this.premium = 0;
	}
	
	@Column(name="Fecha")
	protected String fecha;
	@Column(name="NumRegionEstaciones")
	protected int estacion;
	@Column(name="Magna")
	protected double magna;
	@Column(name="Premium")
	protected double premium;
	@Column(name="Diesel")
	protected double diesel;
	
	

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public int getEstacion() {
		return estacion;
	}

	public void setEstacion(int estacion) {
		this.estacion = estacion;
	}

	public double getMagna() {
		return magna;
	}

	public void setMagna(double magna) {
		this.magna = magna;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public double getDiesel() {
		return diesel;
	}

	public void setDiesel(double diesel) {
		this.diesel = diesel;
	}
	
	
}
