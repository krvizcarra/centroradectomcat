package com.wscentro.modelos;



public class Response {

    protected boolean error;
	protected String msg;
	
	public Response() {
		this.error = true;
		this.msg = "Error al reiniciar Servicio.";
	}

    public boolean getError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    

	
	
}
