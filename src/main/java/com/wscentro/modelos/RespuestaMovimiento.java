package com.wscentro.modelos;



public class RespuestaMovimiento {
	protected int error;
	protected String msg;
	
	public RespuestaMovimiento() {
		this.error = 1;
		this.msg = "";
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


}
