package com.wscentro.modelos;

public interface RespuestaProcedimiento{
	public String getMsg();
	public int getErr();
}