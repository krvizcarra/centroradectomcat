package com.wscentro.modelos;



public class BodyActMov {
    protected String factura;
    protected String movs;

    public BodyActMov(){
        this.factura = "";
        this.movs = "";
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getMovs() {
        return movs;
    }

    public void setMovs(String movs) {
        this.movs = movs;
    }

    
    
}
