package com.wscentro.controladores;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.wscentro.modelos.BodyActMov;
import com.wscentro.modelos.RespuestaEmpresa;
import com.wscentro.modelos.RespuestaMovimiento;
import com.wscentro.modelos.RespuestaResult;
import com.wscentro.modelos.empresa;
import com.wscentro.modelos.objectoCR;
import com.wscentro.servicios.MovimientoServicio;

@RestController
public class MovimientosControlador {
	@Autowired
	protected MovimientoServicio repomovimientos;
	
	protected ObjectMapper mapper;
	Logger logger = LoggerFactory.getLogger(PreciosControlador.class);
	
	
	@RequestMapping(value="/corregirCR",method = RequestMethod.POST)
	public RespuestaMovimiento corregirCR(@RequestBody String input) {
		objectoCR objecto = new Gson().fromJson(input, objectoCR.class); 
		RespuestaMovimiento respuesta = new RespuestaMovimiento();
         if (objecto.getNummov() > 0 && objecto.getProducto() > 0 && objecto.getIeps() > 0 && objecto.getPrecio() > 0) {
        	 RespuestaResult result = repomovimientos.registrarPrecioEstacion(objecto);

        	 respuesta.setError(result.getError());	
        	 respuesta.setMsg(result.getMsg());	

		}else {
			respuesta.setError(1);
			respuesta.setMsg("Faltan parametros");
		}
		 
		return respuesta;
	} 


	@RequestMapping(value="/actualizarMovimientosFactura",method = RequestMethod.POST)
	public RespuestaMovimiento actualizarMovimientosFactura(@RequestBody BodyActMov objecto) {
		RespuestaMovimiento respuesta = new RespuestaMovimiento();
		if (objecto.getFactura() != "") {
			RespuestaResult result = repomovimientos.actualizarMovimientosFactura(objecto);
			respuesta.setError(result.getError());	
			respuesta.setMsg(result.getMsg());	
		}else{
			respuesta.setError(1);
			respuesta.setMsg("Faltan parametros");
		}

		return respuesta;
	} 


	@RequestMapping(value="/duplicarEmpresa",method = RequestMethod.POST)
		public RespuestaEmpresa duplicarEmpresa(@RequestBody empresa empresa) {
			RespuestaEmpresa respuesta = repomovimientos.Config_DuplicarEmpresa(empresa.getEmpresa(), empresa.getCuenta());
		return respuesta;
	} 


	
}
