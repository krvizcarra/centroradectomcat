package com.wscentro.controladores;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.wscentro.modelos.Precio;
import com.wscentro.modelos.Respuesta;
import com.wscentro.modelos.RespuestaProcedimiento;
import com.wscentro.servicios.PrecioServicio;
import com.wscentro.utilidades.Mensajes;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;



@RestController
public class PreciosControlador {
	
	
	@Autowired
	protected PrecioServicio repoprecios;
	
	protected ObjectMapper mapper;
	Logger logger = LoggerFactory.getLogger(PreciosControlador.class);
	
	
	@RequestMapping(value="/obtenerRuta",method = RequestMethod.GET)
	public  String  obtenerRuta() {
		return Mensajes.pathBitacora;
	}
	
	@RequestMapping(value="/obtenerTodosPrecios",method = RequestMethod.GET)
	public  List<Precio>  obtenerTodosPrecios() {
		 List<Precio> respuesta =  repoprecios.findAll();
		return respuesta;
	}
	
	@RequestMapping(value="/obtenerPreciosFecha",method = RequestMethod.POST)
	public Respuesta obtenerPreciosFecha(@RequestBody String input) {
		 Precio precio = new Gson().fromJson(input, Precio.class); 
         Respuesta respuesta = new Respuesta();
         if (precio.getFecha() != "") {
    		 List<Precio> precios =  repoprecios.buscarPorFecha(precio.getFecha());
    		 if (precios.size() > 0) {
 				respuesta.setMsg(String.format("%s registro(s)", precios.size()) );
 				respuesta.setError(0);
 				respuesta.setPrecios(precios);
			}else {
				respuesta.setMsg("No hay precios con esa fecha");
			}
		}else {
			respuesta.setMsg("Faltan parametros");
		}
		 
		return respuesta;
	} 
	
	@RequestMapping(value="/registrarPrecioEstacion",method = RequestMethod.POST)
	public Respuesta registrarPrecioEstacion(@RequestBody String input) {
			Precio precio = new Gson().fromJson(input, Precio.class); 
			Respuesta respuesta = new Respuesta();
			if (precio.getFecha() != "" && precio.getEstacion() != 0 && 
					(precio.getMagna() > 0 || precio.getPremium() > 0 || precio.getDiesel() > 0) ) {
			 	RespuestaProcedimiento insertar = repoprecios.registrarPrecioEstacion(precio);
			 	if (insertar.getErr() == 0) {
			 		respuesta.setError(0);
					respuesta.setMsg(insertar.getMsg());
				}else {
					respuesta.setMsg(insertar.getMsg());
				}
			}else {
				respuesta.setMsg("Faltan parametros");
			}
			
		return respuesta;
	}
	
	@RequestMapping(value="/eliminarPrecioEstacion",method = RequestMethod.POST)
	public Respuesta eliminarPrecioEstacion(@RequestBody String input) {
			Precio precio = new Gson().fromJson(input, Precio.class); 
			Respuesta respuesta = new Respuesta();
			if (precio.getFecha() != "" && precio.getEstacion() != 0) {
			 	RespuestaProcedimiento eliminar = repoprecios.eliminarPrecioEstacion(precio);
			 	if (eliminar.getErr() == 0) {
					respuesta.setError(0);
					respuesta.setMsg(eliminar.getMsg());
				}else {
					respuesta.setMsg(eliminar.getMsg());
				}
			}else {	
				respuesta.setMsg("Faltan parametros");
			}
		return respuesta;
	}
	
}
