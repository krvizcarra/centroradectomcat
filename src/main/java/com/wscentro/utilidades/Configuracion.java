package com.wscentro.utilidades;

/*
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.hibernate.boot.model.naming.ImplicitNamingStrategy;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyLegacyJpaImpl;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Configuration
public class Configuracion {
	private String driver  ;
	private String url ;
	private String user ;
	private String passwd ;

	 	@Bean
	    public DataSource datasource() throws IOException {
	 		leerConfiguraciones();
	        return DataSourceBuilder.create()
	          .driverClassName(this.driver)
	          .url(this.url)
	          .username(this.user)
	          .password(this.passwd)
	          .build(); 
	    }
	 	
	 	
	 	@Bean
	 	public PhysicalNamingStrategy physical() {
	 	    return new PhysicalNamingStrategyStandardImpl();
	 	}
	 	 
	 	@Bean
	 	public ImplicitNamingStrategy implicit() {
	 	    return new ImplicitNamingStrategyLegacyJpaImpl();
	 	}
	 	
	 	private Properties leerConfiguraciones() throws IOException
		{
			Properties propiedades = new Properties();
		    InputStream entrada = null;
			    try{
			    	entrada = new FileInputStream(Mensajes.pathConfig);
			        propiedades.load(entrada);
			        this.driver = propiedades.getProperty("driver");
			        this.url = propiedades.getProperty("url");
			        this.user = propiedades.getProperty("user");
			        this.passwd = propiedades.getProperty("passwd");

			    }catch (IOException e) {
			    	System.out.println(e.getMessage());
			    	Log.LogError(e.getMessage());
			    }
			return propiedades;
		}
	 	

	 
	 
}
*/

