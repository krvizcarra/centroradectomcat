package com.wscentro.utilidades;

import java.io.File;

public class Mensajes {
	public static String PreciosError = "Ocurrio un error al ejecutar el proceso.";
	public static String precioInsertado = "Precio insertado correctamente.";
	public static String precioErrorInsert = "Error al insertar precios.";
	public static String pathBitacora = String.format("%s%s%s",System.getProperty("user.home"),File.separator,"wscentro/Logs/bitacora.txt");
	public static String pathError = String.format("%s%s%s",System.getProperty("user.home"),File.separator,"wscentro/Logs/error.txt");
	public static String pathConfig = String.format("%s/wscentro/Config/configuracion.properties", System.getProperty("user.home") );
	public static String PreciosNoExiste = "No se encontró precios.";
	public static String ErrorEliminarPrecio= "Error al eliminar precios.";
	public static String EliminarPrecio= "Precio eliminado correctamente.";
	public static String ServiciosRadec[] = {"explorer.exe","cmd.exe"};


}
