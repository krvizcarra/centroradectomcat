package com.wscentro.utilidades;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class Log {

	public static final void LogError(String mensaje) throws IOException
	{
		FileWriter archivo;
		
		if (new File(Mensajes.pathError).exists()==false)
        {
			
        	archivo=new FileWriter(new File(Mensajes.pathError),false);
        }
             archivo = new FileWriter(new File(Mensajes.pathError), true);
             Calendar fechaActual = Calendar.getInstance(); 
             String data = String.format("\n %d/%d/%d %d:%d:%d %s %s ",fechaActual.get(Calendar.DAY_OF_MONTH),fechaActual.get(Calendar.MONTH)+1,
            		 fechaActual.get(Calendar.YEAR),fechaActual.get(Calendar.HOUR_OF_DAY),fechaActual.get(Calendar.MINUTE),
            		 fechaActual.get(Calendar.SECOND),Log.class.getSimpleName(),mensaje);
             archivo.write(data);
             archivo.close(); 
     }
	
	public static final void LogBitacora(String mensaje) throws IOException
	{	
		FileWriter archivo;
		
		if (new File(Mensajes.pathBitacora).exists()==false)
        {
			
        	archivo=new FileWriter(new File(Mensajes.pathBitacora),false);
        }
             archivo = new FileWriter(new File(Mensajes.pathBitacora), true);
             Calendar fechaActual = Calendar.getInstance(); 
             String data = String.format("\n %d/%d/%d %d:%d:%d %s %s ",fechaActual.get(Calendar.DAY_OF_MONTH),fechaActual.get(Calendar.MONTH)+1,
            		 fechaActual.get(Calendar.YEAR),fechaActual.get(Calendar.HOUR_OF_DAY),fechaActual.get(Calendar.MINUTE),
            		 fechaActual.get(Calendar.SECOND),Log.class.getSimpleName(),mensaje);
             archivo.write(data);
             archivo.close(); 
     }
}
