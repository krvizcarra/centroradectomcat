package com.wscentro.repositorios;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

 

import com.wscentro.modelos.Precio;
import com.wscentro.modelos.RespuestaResult;

public interface IMovimientoRepositorios extends JpaRepository<Precio, Long>{
	
	@Query(nativeQuery = true, value= "EXEC WsCorregirCR :nummov,:producto,:ieps,:precio")
	public RespuestaResult registrarPrecioEstacion(@Param("nummov") int nummov,
														  @Param("producto") int producto,
														  @Param("ieps") double ieps,
														  @Param("precio") double precio);

	@Query(nativeQuery = true, value= "EXEC WsactualizarMovimientosFactura :factura,:movs")
	public RespuestaResult actualizarMovimientosFactura(@Param("factura") String factura,
														@Param("movs") String movs);		
														
														
	@Query(nativeQuery = true, value= "EXEC Config_DuplicarEmpresa  :empresa,:cuenta")
	public String Config_DuplicarEmpresa(@Param("empresa") String empresa,@Param("cuenta") String cuenta);			

	
}
