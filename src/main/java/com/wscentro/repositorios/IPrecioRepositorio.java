package com.wscentro.repositorios;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.wscentro.modelos.Precio;
import com.wscentro.modelos.RespuestaProcedimiento;

public interface IPrecioRepositorio extends JpaRepository<Precio, Long>{

	@Query(nativeQuery = true, value= "EXEC buscarPorFecha :fecha ")
	public List<Precio> buscarPorFecha(@Param("fecha") String fecha);
	
	@Query(nativeQuery = true, value= "EXEC registrarPrecioEstacion :fecha,:estacion,:magna,:premium,:diesel ")
	public RespuestaProcedimiento registrarPrecioEstacion(@Param("fecha") String fecha,
														  @Param("estacion") int estacion,
														  @Param("magna") double magna,
														  @Param("premium") double premium,
														  @Param("diesel") double diesel);
	
	@Query(nativeQuery = true, value= "EXEC eliminarPrecioEstacion :fecha,:estacion ")
	public RespuestaProcedimiento eliminarPrecioEstacion(@Param("fecha") String fecha,@Param("estacion") int estacion);
	
}
